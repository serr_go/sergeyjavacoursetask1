package com.sergey.javacourse.task1;

import com.google.gson.*;

public class JsnParser {
    public WeatherStorage parseRequest(String response) {
        Gson gson = new GsonBuilder().create();
        WeatherStorage tmpWeatherStorage = gson.fromJson(response, WeatherStorage.class);

        return tmpWeatherStorage;
    }
}
