package com.sergey.javacourse.task1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class XMLPrinter implements Printer {
    @Override
    public void print(WeatherStorage weatherForGomel) {
        WeatherLocale locale = new WeatherLocale();
        FileWriter file = null;
        ArrayList<Weather.Results.Channel> channel = weatherForGomel.getStorage().getQuery().getResults().getChannel();

        try {
            file = new FileWriter("Weather.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for ( Weather.Results.Channel rec: channel) {
            Weather.Results.Channel.Item.Forecast forecast = rec.getItem().getForecast();
            String xmlString = (
                    new StringBuffer("<date>")
                            .append(forecast.getDate())
                            .append("</date>")
                            .append("<low>")
                            .append(forecast.getMinTempCel())
                            .append("</low>")
                            .append("<high>")
                            .append(forecast.getMaxTempCel())
                            .append("</high>")
                            .append("<text>")
                            .append(locale.getLocaleText(Integer.toString(forecast.getCode())))
                            .append("</text>")
                            .toString()
            );
            try {
                file.write(xmlString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
