package com.sergey.javacourse.task1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONObject;

public class JsnPrinter implements Printer {
    @Override
    public void print(WeatherStorage weatherForGomel){
        JSONObject obj = new JSONObject();
        WeatherLocale locale = new WeatherLocale();
        FileWriter file = null;

        try {
            file = new FileWriter("Weather.json");
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Weather.Results.Channel> channel = weatherForGomel.getStorage().getQuery().getResults().getChannel();

        for ( Weather.Results.Channel rec: channel){
            Weather.Results.Channel.Item.Forecast forecast = rec.getItem().getForecast();
            obj.put("date", forecast.getDate());
            obj.put("low", forecast.getMinTempCel());
            obj.put("high", forecast.getMaxTempCel());
            obj.put("text", locale.getLocaleText(Integer.toString(forecast.getCode())));

            try {
                file.write(obj.toJSONString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
