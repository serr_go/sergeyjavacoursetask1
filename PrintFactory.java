package com.sergey.javacourse.task1;

public class PrintFactory {
    public Printer getPrinter(String format) {
        if (format.equals("CSV"))
            return new CSVPrinter();
        else if (format.equals("JSON"))
            return new JsnPrinter();
        else if (format.equals("XML"))
            return new XMLPrinter();
        return null;
    }
}
