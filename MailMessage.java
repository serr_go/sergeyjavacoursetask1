package com.sergey.javacourse.task1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MailLetterData {
    private ArrayList<ArrayList> dlist = new ArrayList<>();
    private ArrayList<String> recipientEmails = new ArrayList<>();
    private ArrayList<String> weatherFormats = new ArrayList<>();
    private ArrayList<String> recipientCities = new ArrayList<>();
    private int numOfRecipients;

    public MailLetterData() {
        dlist.add(recipientEmails);
        dlist.add(weatherFormats);
        dlist.add(recipientCities);
        FileReader file = null;
        try {
            file = new FileReader("dlist.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scan = new Scanner(file);

        int line = 0;
        while (scan.hasNextLine()) {
            ArrayList <String> tmpList = dlist.get(line);
            String str = scan.nextLine();
            for (String  word : str.split(";")) {
                tmpList.add(word);
            }
            line++;
        }
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        numOfRecipients = dlist.get(0).size();
    }


    public int getNumOfRecipients() {
        return numOfRecipients;
    }

    public String getCity(int recipient) {
        ArrayList <String> citiesList = dlist.get(2);
        return citiesList.get(recipient);
    }

    public String getMessage(int recipient) {
        ArrayList <String> citiesList = dlist.get(2);
        ArrayList <String> localesList = dlist.get(1);
        WeatherStorage weatherForCity = new WeatherStorage(citiesList.get(recipient));
        WeatherLocale locale = new WeatherLocale(localesList.get(recipient));

        ArrayList<Weather.Results.Channel> channel = weatherForCity.getStorage().getQuery().getResults().getChannel();

        String msg = "";
        for ( Weather.Results.Channel rec: channel){
            Weather.Results.Channel.Item.Forecast forecast = rec.getItem().getForecast();

            msg += forecast.getDay()+" "+
                    forecast.getDate()+" Max:"+
                    forecast.getMaxTempCel()+" Min:"+
                    forecast.getMinTempCel()+" "+
                    locale.getLocaleText(Integer.toString(forecast.getCode()));
        }
        return msg;
    }

    public String getRecipientEmail(int recipient) {
        ArrayList <String> emailsList = dlist.get(0);
        return emailsList.get(recipient);
    }
}
