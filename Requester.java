package com.sergey.javacourse.task1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;

public class Requester {
    private final String USER_AGENT = "Mozilla/5.0";

    public String sendGetRequest(String city) throws IOException {
        //take request url form file url.txt
        //select item.forecast from weather.forecast where woeid in (select woeid from geo.places(1) where text="")
        FileReader file = null;
        try {
            file = new FileReader("url.txt");
        } catch (FileNotFoundException e) {
            System.out.println("Error reading URL from file.");
            e.printStackTrace();
        }
        Scanner scan = new Scanner(file);
        StringBuffer url = new StringBuffer(scan.nextLine());

        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        url.insert(getIndexOfCityInURL(url),city);

        //send GET request
        URL obj = null;
        try {
            obj = new URL(url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            con.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        con.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = null;
        try {
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null){
            response.append(inputLine);
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }

    //use regex in next patch
    private int getIndexOfCityInURL(StringBuffer url){
        //City name situated between quotes("")
        //%22 is code of quotes in reqest
        int indexOfQuotes = url.indexOf("%22")+3;
        if (indexOfQuotes != 0){
            return indexOfQuotes;
        }
        else {
            System.out.println("URL error. Can't make correct URL.");
        }
        return 0;
    }
}
