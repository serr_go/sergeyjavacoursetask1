package com.sergey.javacourse.task1;

import java.io.IOException;

public class WeatherStorage {
    private Weather query;
    private WeatherStorage storage;

    public WeatherStorage(String city){
        Requester request = new Requester();
        JsnParser parser = new JsnParser();

        try {
            storage = parser.parseRequest(request.sendGetRequest(city));
        } catch (IOException e) {
            System.out.println("Cannot create WeatherStorage object.");
            e.printStackTrace();
        }
    }

    public Weather getQuery() {
        return query;
    }

    public WeatherStorage getStorage() {
        return storage;
    }
}
