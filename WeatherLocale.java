package com.sergey.javacourse.task1;

import java.io.FileInputStream;
import java.util.Properties;
import java.io.IOException;
import java.util.Locale;

public class WeatherLocale {
    private String weatherLocale = Locale.getDefault().toString();

    public WeatherLocale(String... weatherLocale) {
        if (weatherLocale[0] != null) {
            this.weatherLocale = weatherLocale[0];
        }
    }

    public String getLocaleText(String code) {
        FileInputStream pFile;
        Properties locale = new Properties();

        switch (weatherLocale){
            case "ru_RU": {
                try {
                    pFile = new FileInputStream("wthRU.properties");
                    locale.load(pFile);
                    pFile.close();
                    return locale.getProperty(code);
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;
            }
            case "en_EN":{
                try {
                    pFile = new FileInputStream("wthEN.properties");
                    locale.load(pFile);
                    pFile.close();
                    return locale.getProperty(code);
                } catch (IOException e){
                    e.printStackTrace();
                }
                break;
            }
            default:{
                break;
            }
        }
        return null;
    }
}
