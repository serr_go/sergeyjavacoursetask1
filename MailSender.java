package com.sergey.javacourse.task1;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class MailSender {

    public void sendWeather(){
        String from = "sendFromMailAddress";
        String host = "smtp.yourisp.net";
        int port = 123;

        Properties props = new Properties();

        props.put("mail.smtp.host", host);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("login", "password");
            }
        });

        MailLetterData letter = new MailLetterData();
        for (int recipient = 0; recipient < letter.getNumOfRecipients(); recipient++) {
            try {
                Message msg = new MimeMessage(session);

                msg.setFrom(new InternetAddress(from));
                InternetAddress[] address = {new InternetAddress(letter.getRecipientEmail(recipient))};
                msg.setRecipients(Message.RecipientType.TO, address);
                msg.setSubject("Weather for " + letter.getCity(recipient));
                msg.setSentDate(new Date());

                msg.setText(letter.getMessage(recipient));

                Transport.send(msg);
            } catch (MessagingException mex) {
                mex.printStackTrace();
            }
        }
    }
}