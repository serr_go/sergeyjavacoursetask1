package com.sergey.javacourse.task1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class CSVPrinter implements Printer{
    @Override
    public void print(WeatherStorage weatherForGomel){
        WeatherLocale locale = new WeatherLocale();
        FileWriter printer = null;

        try {
            printer = new FileWriter("Weather.csv", false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Weather.Results.Channel> channel = weatherForGomel.getStorage().getQuery().getResults().getChannel();

        for ( Weather.Results.Channel rec: channel){
            Weather.Results.Channel.Item.Forecast forecast = rec.getItem().getForecast();
            try {
                printer.write(forecast.getDay()+";"+
                        forecast.getDate()+";"+
                        forecast.getMaxTempCel()+";"+
                        forecast.getMinTempCel()+";"+
                        locale.getLocaleText(Integer.toString(forecast.getCode())));
                printer.write(System.lineSeparator());
            } catch (IOException e) {
                System.out.println("Error printing in file.");
                e.printStackTrace();
            }
        }

        try {
            printer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
